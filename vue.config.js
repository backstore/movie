//vue.config.js
module.exports = {
    devServer: {
        baseUrl: '/',
        proxy: {
            '/api': {
                target: 'http://172.21.169.33:3000/',
                ws: true,
                changeOrigin: true
            },
            '/foo': {
                target: 'http://130.10.7.48:7000/',
                ws: true,
                changeOrigin: true
            }
        }
    }
}